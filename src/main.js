// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import store from './vuex/Store'
import moment from 'moment'
import VueMomentJS from 'vue-momentjs'
import VueNotifications from 'vue-notifications'
import miniToastr from 'mini-toastr'
import VueResource from 'vue-resource'
import Vuex from 'vuex'
// import * as VueGoogleMaps from 'vue2-google-maps'
import VuePassword from 'vue-password'
import VueGmaps from 'vue-gmaps'
import VueGeolocation from 'vue-browser-geolocation'
import Snotify from 'vue-snotify'
import VueGoodTable from 'vue-good-table'

Vue.config.productionTip = false
Vue.use(VueMomentJS, moment)
Vue.use(VueResource)
Vue.use(Vuex)
Vue.use(VueGeolocation)
Vue.use(Snotify)
Vue.use(VueGmaps, {
  key: 'AIzaSyDyhPQ3p-isxohYBO4SREmk2-Q_XSIMhDM'
})
Vue.use(VueGoodTable)

//* eslint-disable no-new */
Vue.http.interceptors.push(function (request, next) {
  // modify headers
  request.headers.set('Accept', 'application/json')
  // request.headers.set('Access-Control-Allow-Origin', '*')
  // request.headers.set('Access-Control-Request-Method', '*')
  request.headers.set('Authorization', 'Bearer ' + localStorage.getItem('access_token'))
  // continue to next interceptor
  next()
})

// If using mini-toastr, provide additional configuration
const toastTypes = {
  success: 'success',
  error: 'error',
  info: 'info',
  warn: 'warn'
}

// Vue.use(VueGoogleMaps, VueGmaps, {
//   load: {
//     key: 'AIzaSyDyhPQ3p-isxohYBO4SREmk2-Q_XSIMhDM',
//     libraries: 'places' // This is required if you use the Autocomplete plugin
//     // OR: libraries: 'places,drawing'
//     // OR: libraries: 'places,drawing,visualization'
//     // (as you require)
//   }
// })

miniToastr.init({types: toastTypes})

// Here we setup messages output to `mini-toastr`
function toast ({title, message, type, timeout, cb}) {
  return miniToastr[type](message, title, timeout, cb)
}

// Binding for methods .success(), .error() and etc. You can specify and map your own methods here.
// Required to pipe our output to UI library (mini-toastr in example here)
// All not-specified events (types) would be piped to output in console.
const options = {
  success: toast,
  error: toast,
  info: toast,
  warn: toast
}

Vue.use(VueNotifications, options)// VueNotifications have auto install but if we want to specify options we've got to do it manually.

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  template: '<App/>',
  components: { App, VuePassword }
})
