import Vue from 'vue'
import {institutionUrls} from '../defaults/.url'
import router from '../../router/index'
import VueNotifications from 'vue-notifications'

export default {
  get_institution_types (context) {
    Vue.http.get(institutionUrls.getInstitutionTypes).then(function (response) {
      context.commit('GET_INSTITUTION_TYPES', response.data)
      context.dispatch('loading_false')
    })
  },
  get_deleted_starts (context) {
    Vue.http.get(institutionUrls.getDeletedStart).then(function (response) {
      context.commit('GET_DELETED_INSTITUTIONS', response.data)
      context.dispatch('loading_false')
    })
  },
  getInstitution (context, institutionId) {
    Vue.http.get(institutionUrls.getInstitution + institutionId).then(function (response) {
      context.commit('GET_INSTITUTION', response.data)
      context.dispatch('loading_false')
    })
  },
  post_institution (context, data) {
    context.errors = []
    const postData = {
      institution_type_id: data.type,
      name: data.name,
      website: data.website,
      email: data.email,
      phone_number: data.phone_number
    }
    Vue.http.post(institutionUrls.postInstitution, postData).then(function (response) {
      context.dispatch('get_all_starts')
      router.push({
        name: 'Index'
      })
      VueNotifications.success({message: 'Institution Successfully registered. Check your email for farther details'})
      context.dispatch('loading_false')
    }).catch(function (error) {
      var size = error.data.errors.length
      if (size > 0) {
        for (var i = 0; i < size; i++) {
          VueNotifications.error({message: error.data.errors[i].message})
        }
      }

      context.commit('UPDATE_ERRORS', error.data)
      context.dispatch('loading_false')
      // console.log(error.data)
    })
  },
  post_branch (context, data) {
    context.errors = []
    const postData = {
      institution_id: data.institution,
      name: data.name,
      phone_number: data.phone_number,
      email: data.email,
      location: data.place.name,
      tag_line: data.tag_line,
      description: data.description
    }
    Vue.http.post(institutionUrls.postBranch, postData).then(function (response) {
      context.dispatch('get_branches', data.institution)
      router.push({
        name: 'Branches'
      })
      VueNotifications.success({message: response.data.message})
      context.dispatch('loading_false')
    }).catch(function (error) {
      var size = error.data.errors.length
      if (size > 0) {
        for (var i = 0; i < size; i++) {
          VueNotifications.error({message: error.data.errors[i].message})
        }
      }
      context.commit('UPDATE_ERRORS', error.data)
      context.dispatch('loading_false')
      // console.log(error.data)
    })
  },
  get_branches (context, institutionId) {
    Vue.http.get(institutionUrls.getBranches + institutionId).then(function (response) {
      context.commit('GET_BRANCHES', response.data)
      context.dispatch('loading_false')
    })
  },
  get_near_branches (context) {
    Vue.http.get(institutionUrls.getNearBranches).then(function (response) {
      context.commit('GET_BRANCHES', response.data)
      context.dispatch('loading_false')
    })
  },
  get_branch (context, branchId) {
    Vue.http.get(institutionUrls.getBranch + branchId).then(function (response) {
      context.commit('GET_BRANCH', response.data)
      context.dispatch('loading_false')
    })
  },
  update_start (context, data) {
    context.errors = []
    const postData = {
      first_name: data.first_name
    }
    Vue.http.post(institutionUrls.postStart, postData).then(function () {
      context.dispatch('get_all_starts')
      router.push({
        name: data.redirect_url
      })
      VueNotifications.success({message: 'Success'})
      context.dispatch('loading_false')
    }).catch(function (error) {
      context.commit('UPDATE_ERRORS', error.data)
      context.dispatch('loading_false')
      VueNotifications.error({message: 'There was an error'})
      // console.log(error.data)
    })
  },
  delete_start (context, publicId) {
    Vue.http.get(institutionUrls.deleteStart + publicId).then(function (response) {
      alert(response.data.message)
      context.commit('GET_DELETED_INSTITUTIONS', response.data)
      router.push({
        name: 'Module.DeletedStarts'
      })
      VueNotifications.info({message: response.data.message})
      context.dispatch('loading_false')
    })
  },
  restoreStart (context, publicId) {
    Vue.http.get(institutionUrls.restoreStart + publicId).then(function (response) {
      alert(response.data.message)
      context.commit('GET_ALL_INSTITUTIONS', response.data)
      router.push({
        name: 'Module.Starts'
      })
      VueNotifications.info({message: response.data.message})
      context.dispatch('loading_false')
    })
  }
}
