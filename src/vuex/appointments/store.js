import mutations from './mutations'
import actions from './actions'

const state = {
  all_starts: [],
  starts: [],
  start: [],
  deleted_starts: [],
  appointments: [],
  branch_count: []
}

export default {
  state, mutations, actions
}
