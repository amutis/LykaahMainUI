export default {
  loading_true (context) {
    context.commit('CHANGE_LOADING', true)
  },
  loading_false (context) {
    context.commit('CHANGE_LOADING', false)
  }
}
