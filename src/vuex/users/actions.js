import Vue from 'vue'
import {userUrls} from '../defaults/.url'
import router from '../../router/index'
import VueNotifications from 'vue-notifications'

export default {
  get_all_users (context) {
    Vue.http.get(userUrls.getAllUsers).then(function (response) {
      context.commit('GET_ALL_STARTS', response.data)
      context.dispatch('loading_false')
    })
  },
  get_users (context, publicId) {
    Vue.http.get(userUrls.getUsers + publicId).then(function (response) {
      context.commit('GET_STARTS', response.data)
      context.dispatch('loading_false')
    })
  },
  update_branch_status (context, publicId) {
    Vue.http.get(userUrls.updateManagerStatus + publicId).then(function (response) {
      VueNotifications.success({message: 'Success'})
      router.push({
        name: 'Branches'
      })
      context.dispatch('loading_false')
    })
  },
  get_branch_users (context, publicId) {
    Vue.http.get(userUrls.getBranchUsers + publicId).then(function (response) {
      context.commit('GET_USERS', response.data)
      context.dispatch('loading_false')
    })
  },
  get_deleted_users (context) {
    Vue.http.get(userUrls.getDeletedUser).then(function (response) {
      context.commit('GET_DELETED_STARTS', response.data)
      context.dispatch('loading_false')
    })
  },
  get_user (context, userId) {
    Vue.http.get(userUrls.getUser + userId).then(function (response) {
      context.commit('GET_START', response.data)
      context.dispatch('loading_false')
    })
  },
  count_users (context) {
    Vue.http.get(userUrls.countUsers).then(function (response) {
      context.commit('COUNT_USERS', response.data)
      context.dispatch('loading_false')
    })
  },
  post_institution_user (context, data) {
    context.errors = []
    const postData = {
      first_name: data.first_name,
      last_name: data.last_name,
      email: data.email,
      password: data.password,
      phone_number: data.phone_number,
      institution_id: data.institution
    }
    Vue.http.post(userUrls.postInstitutionUser, postData).then(function () {
      router.push({
        name: data.redirect_url
      })
      VueNotifications.success({message: 'Success'})
      context.dispatch('loading_false')
    }).catch(function (error) {
      context.commit('UPDATE_ERRORS', error.data)
      context.dispatch('loading_false')
      // console.log(error.data)
    })
  },
  post_branch_manager (context, data) {
    const postData = {
      first_name: data.first_name,
      last_name: data.last_name,
      email: data.email,
      phone_number: data.phone_number,
      institution_id: data.institution,
      branch_id: data.branch
    }
    Vue.http.post(userUrls.postBranchUser, postData).then(function () {
      context.dispatch('update_branch_status', data.branch)
    }).catch(function (error) {
      context.commit('UPDATE_ERRORS', error.data)
      context.dispatch('loading_false')
      // console.log(error.data)
    })
  },
  register_user  (context, data) {
    const postData = {
      first_name: data.first_name,
      last_name: data.last_name,
      email: data.email,
      phone_number: data.phone_number,
      password: data.password
    }
    Vue.http.post(userUrls.postUser, postData).then(function () {
      VueNotifications.error({message: 'Your account has successfully been created.'})
    }).catch(function (error) {
      VueNotifications.error({message: error.data.message})
      context.commit('UPDATE_ERRORS', error.data)
      context.dispatch('loading_false')
      // console.log(error.data)
    })
  },
  update_user (context, data) {
    context.errors = []
    const postData = {
      first_name: data.first_name
    }
    Vue.http.post(userUrls.postUser, postData).then(function () {
      context.dispatch('get_all_users')
      router.push({
        name: data.redirect_url
      })
      VueNotifications.success({message: 'Success'})
      context.dispatch('loading_false')
    }).catch(function (error) {
      context.commit('UPDATE_ERRORS', error.data)
      context.dispatch('loading_false')
      VueNotifications.error({message: 'There was an error'})
      // console.log(error.data)
    })
  },
  delete_user (context, publicId) {
    Vue.http.get(userUrls.deleteUser + publicId).then(function (response) {
      alert(response.data.message)
      context.commit('GET_DELETED_STARTS', response.data)
      router.push({
        name: 'Module.DeletedUsers'
      })
      VueNotifications.info({message: response.data.message})
      context.dispatch('loading_false')
    })
  },
  restoreUser (context, publicId) {
    Vue.http.get(userUrls.restoreUser + publicId).then(function (response) {
      alert(response.data.message)
      context.commit('GET_ALL_STARTS', response.data)
      router.push({
        name: 'Module.Users'
      })
      VueNotifications.info({message: response.data.message})
      context.dispatch('loading_false')
    })
  }
}
