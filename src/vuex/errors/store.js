import mutations from './mutations'

const state = {
  all_errors: []
}

export default {
  state, mutations
}
