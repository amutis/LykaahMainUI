import mutations from './mutations'
import actions from './actions'

const state = {
  institution_types: [],
  institution: [],
  start: [],
  deleted_starts: [],
  branches: [],
  branch: [],
  services: []
}

export default {
  state, mutations, actions
}
