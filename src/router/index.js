import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/components/home/Home'
import Index from '@/components/home/Index'
import InstitutionHome from '@/components/institutions/Home'
import Beauty from '@/components/institutions/Beauty'
import Results from '@/components/institutions/Results'
import Branch from '@/components/institutions/Branch'
import Service from '@/components/institutions/Service'
import Booking from '@/components/institutions/Booking'
import Register from '@/components/institutions/Register'
import CompleteRegistration from '@/components/institutions/CompleteRegistration'
import SignUpLogin from '@/components/home/SignUpLogin'

import Main from '@/components/dashboard/Main'
import Dashboard from '@/components/dashboard/Dashboard'
import Calendar from '@/components/dashboard/Calendar'
import Presentation from '@/components/dashboard/Presentation'

// Branches
import AddBranch from '@/components/dashboard/branches/AddBranch'
import SingleBranch from '@/components/dashboard/branches/Branch'
import Branches from '@/components/dashboard/branches/Branches'

// Employees
import AddManager from '@/components/dashboard/users/AddManager'

// Descriptions
import BookingDescription from '@/components/dashboard/Descriptions/Booking'
import Sms from '@/components/dashboard/Descriptions/Sms'

// Service
import AddService from '@/components/dashboard/services/AddService'
import Services from '@/components/dashboard/services/Services'
import SingleService from '@/components/dashboard/services/Service'

// Appointments
import NewAppointments from '@/components/dashboard/appointments/NewAppointments'
import Appointments from '@/components/dashboard/appointments/Appointments'
import ConfirmPayment from '@/components/dashboard/appointments/ConfirmPayment'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home,
      children: [
        {
          path: '/',
          name: 'Index',
          component: Index
        },
        {
          path: 'signup',
          name: 'SignUpLogin',
          component: SignUpLogin
        }
      ]
    },
    {
      path: '/on-the-go',
      name: 'Main',
      component: Main,
      children: [
        {
          path: '/',
          name: 'Dashboard',
          component: Dashboard
        },
        {
          path: 'calendar',
          name: 'Calendar',
          component: Calendar
        },
        {
          path: 'branches',
          name: 'Branches',
          component: Branches
        },
        {
          path: 'branch/:branch_id',
          name: 'SingleBranch',
          component: SingleBranch
        },
        {
          path: 'add/branch',
          name: 'AddBranch',
          component: AddBranch
        },
        {
          path: 'add/manager/:branch_id',
          name: 'AddManager',
          component: AddManager
        },
        {
          path: 'add/service',
          name: 'AddService',
          component: AddService
        },
        {
          path: 'services',
          name: 'Services',
          component: Services
        },
        {
          path: 'service/:service_id',
          name: 'Service',
          component: SingleService
        },
        {
          path: 'new/appointments',
          name: 'NewAppointments',
          component: NewAppointments
        },
        {
          path: 'old/appointments',
          name: 'Appointments',
          component: Appointments
        },
        {
          path: 'presentation',
          name: 'Presentation',
          component: Presentation
        },
        {
          path: 'confirm-payment',
          name: 'ConfirmPayment',
          component: ConfirmPayment
        },
        {
          path: 'info/online-booking',
          name: 'BookingDescription',
          component: BookingDescription
        },
        {
          path: 'info/sms-communication',
          name: 'Sms',
          component: Sms
        }
      ]
    },
    {
      path: '/village',
      name: 'Institutions',
      component: Beauty,
      children: [
        {
          path: '/',
          name: 'Institution.Index',
          component: InstitutionHome
        },
        {
          path: 'search/results',
          name: 'Results',
          component: Results
        },
        {
          path: 'branch/:branch_id',
          name: 'Branch',
          component: Branch
        },
        {
          path: 'service/:service_id',
          name: 'Service',
          component: Service
        },
        {
          path: 'book/service/:service_id',
          name: 'BookService',
          component: Booking
        },
        {
          path: 'book/:branch_id',
          name: 'Book',
          component: Booking
        },
        {
          path: 'registration',
          name: 'Register',
          component: Register
        },
        {
          path: 'complete-registration/:institution_id',
          name: 'CompleteRegistration',
          component: CompleteRegistration
        }
      ]
    }
  ],
  mode: 'history'
})
