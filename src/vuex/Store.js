import Vue from 'vue'
import Vuex from 'vuex'
import institutionStore from './institution/store'
import searchStore from './search/store'
import errorStore from './errors/store'
import userStore from './users/store'
import authStore from './authentication/store'
import loadingStore from './loading/store'
import appointmentStore from './appointments/store'
import serviceStore from './services/store'

Vue.config.productionTip = false
Vue.use(Vuex)
Vue.config.debug = true

const debug = process.env.NODE_ENV !== 'production'

export default new Vuex.Store({
  modules: {
    institutionStore,
    searchStore,
    errorStore,
    userStore,
    authStore,
    loadingStore,
    appointmentStore,
    serviceStore
  },
  strict: debug
})
