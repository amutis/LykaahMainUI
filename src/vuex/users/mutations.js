export default {
  GET_STARTS (state, data) {
    state.starts = data.data
  },
  GET_ALL_STARTS (state, data) {
    state.all_starts = data.data
  },
  GET_START (state, data) {
    state.start = data.data
  },
  GET_DELETED_STARTS (state, data) {
    state.deleted_starts = data.data
  },
  GET_USERS (state, data) {
    state.users = data.data
  },
  COUNT_USERS (state, data) {
    state.count_users = data.data
  }
}
