import Vue from 'vue'
import {appointmentUrls} from '../defaults/.url'
import router from '../../router/index'
import VueNotifications from 'vue-notifications'

export default {
  get_new_appointments (context, publicId) {
    Vue.http.get(appointmentUrls.getNewAppointments + publicId).then(function (response) {
      context.commit('GET_NEW_APPOINTMENTS', response.data)
      context.dispatch('loading_false')
    })
  },
  get_old_appointments (context, publicId) {
    Vue.http.get(appointmentUrls.getOldAppointments + publicId).then(function (response) {
      context.commit('GET_OLD_APPOINTMENTS', response.data)
      context.dispatch('loading_false')
    })
  },
  get_starts (context, publicId) {
    Vue.http.get(appointmentUrls.getStarts + publicId).then(function (response) {
      context.commit('GET_STARTS', response.data)
      context.dispatch('loading_false')
    })
  },
  count_branch_appointments_today (context) {
    Vue.http.get(appointmentUrls.branchCount + localStorage.getItem('branch')).then(function (response) {
      context.commit('BRANCH_COUNT', response.data)
      context.dispatch('loading_false')
    })
  },
  get_deleted_starts (context) {
    Vue.http.get(appointmentUrls.getDeletedStart).then(function (response) {
      context.commit('GET_DELETED_STARTS', response.data)
      context.dispatch('loading_false')
    })
  },
  get_start (context, startId) {
    Vue.http.get(appointmentUrls.getStart + startId).then(function (response) {
      context.commit('GET_START', response.data)
      context.dispatch('loading_false')
    })
  },
  post_appointment (context, data) {
    context.errors = []
    const postData = {
      branch_id: data.branch_id,
      service_id: data.service_id,
      user_id: localStorage.getItem('lkjhgfdsa'),
      date: data.formated_date,
      time: data.selected_time.HH + ':' + data.selected_time.mm + ':00',
      mpesa_code: data.mpesa_code
    }
    Vue.http.post(appointmentUrls.postAppointment, postData).then(function () {
      router.push({
        name: 'Institution.Index'
      })
      VueNotifications.success({message: 'Success'})
      context.dispatch('loading_false')
    }).catch(function (error) {
      context.commit('UPDATE_ERRORS', error.data)
      context.dispatch('loading_false')
      // console.log(error.data)
    })
  },
  confirm_payment (context, mpesaCode) {
    const postData = {
      mpesa_code: mpesaCode
    }
    Vue.http.post(appointmentUrls.postCode, postData).then(function (response) {
      router.push({
        name: 'NewAppointments'
      })
      VueNotifications.success({message: response.data.message})
      context.dispatch('loading_false')
    }).catch(function (error) {
      context.commit('UPDATE_ERRORS', error.data)
      VueNotifications.error({message: error.data.message})
      context.dispatch('loading_false')
      // console.log(error.data)
    })
  },
  update_start (context, data) {
    context.errors = []
    const postData = {
      first_name: data.first_name
    }
    Vue.http.post(appointmentUrls.postStart, postData).then(function () {
      context.dispatch('get_all_starts')
      router.push({
        name: data.redirect_url
      })
      VueNotifications.success({message: 'Success'})
      context.dispatch('loading_false')
    }).catch(function (error) {
      context.commit('UPDATE_ERRORS', error.data)
      context.dispatch('loading_false')
      VueNotifications.error({message: 'There was an error'})
      // console.log(error.data)
    })
  },
  delete_start (context, publicId) {
    Vue.http.get(appointmentUrls.deleteStart + publicId).then(function (response) {
      alert(response.data.message)
      context.commit('GET_DELETED_STARTS', response.data)
      router.push({
        name: 'Module.DeletedStarts'
      })
      VueNotifications.info({message: response.data.message})
      context.dispatch('loading_false')
    })
  },
  restoreStart (context, publicId) {
    Vue.http.get(appointmentUrls.restoreStart + publicId).then(function (response) {
      alert(response.data.message)
      context.commit('GET_ALL_STARTS', response.data)
      router.push({
        name: 'Module.Starts'
      })
      VueNotifications.info({message: response.data.message})
      context.dispatch('loading_false')
    })
  }
}
