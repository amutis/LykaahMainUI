import mutations from './mutations'
import actions from './actions'

const state = {
  all_starts: [],
  starts: [],
  start: [],
  deleted_starts: [],
  users: [],
  count_users: []
}

export default {
  state, mutations, actions
}
