export default {
  GET_INSTITUTION_TYPES (state, data) {
    state.institution_types = data.data
  },
  GET_INSTITUTION (state, data) {
    state.institution = data.data[0]
  },
  GET_START (state, data) {
    state.start = data.data
  },
  GET_DELETED_STARTS (state, data) {
    state.deleted_starts = data.data
  },
  GET_BRANCHES (state, data) {
    state.branches = data.data
  },
  GET_BRANCH (state, data) {
    state.branch = data.data[0]
  }
}
