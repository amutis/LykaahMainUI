import Vue from 'vue'
import {serviceUrls} from '../defaults/.url'
import router from '../../router/index'
import VueNotifications from 'vue-notifications'

export default {
  get_all_starts (context) {
    Vue.http.get(serviceUrls.getAllStarts).then(function (response) {
      context.commit('GET_ALL_STARTS', response.data)
      context.dispatch('loading_false')
    })
  },
  get_starts (context, publicId) {
    Vue.http.get(serviceUrls.getStarts + publicId).then(function (response) {
      context.commit('GET_STARTS', response.data)
      context.dispatch('loading_false')
    })
  },
  get_deleted_starts (context) {
    Vue.http.get(serviceUrls.getDeletedStart).then(function (response) {
      context.commit('GET_DELETED_STARTS', response.data)
      context.dispatch('loading_false')
    })
  },
  get_services (context, publicId) {
    Vue.http.get(serviceUrls.getBranchServices + publicId).then(function (response) {
      context.commit('GET_SERVICES', response.data)
      context.dispatch('loading_false')
    })
  },
  post_service (context, data) {
    context.errors = []
    const postData = {
      name: data.name,
      price: data.price,
      description: data.description,
      branch_id: data.branch_id
    }
    Vue.http.post(serviceUrls.postService, postData).then(function () {
      context.dispatch('get_services', data.branch_id)
      router.push({
        name: 'Services'
      })
      VueNotifications.success({message: 'Success'})
      context.dispatch('loading_false')
    }).catch(function (error) {
      var size = error.data.errors.length
      if (size > 0) {
        for (var i = 0; i < size; i++) {
          VueNotifications.error({message: error.data.errors[i].message})
        }
      }
      context.commit('UPDATE_ERRORS', error.data)
      context.dispatch('loading_false')
      // console.log(error.data)
    })
  },
  update_start (context, data) {
    context.errors = []
    const postData = {
      first_name: data.first_name
    }
    Vue.http.post(serviceUrls.postStart, postData).then(function () {
      context.dispatch('get_all_starts')
      router.push({
        name: data.redirect_url
      })
      VueNotifications.success({message: 'Success'})
      context.dispatch('loading_false')
    }).catch(function (error) {
      context.commit('UPDATE_ERRORS', error.data)
      context.dispatch('loading_false')
      VueNotifications.error({message: 'There was an error'})
      // console.log(error.data)
    })
  },
  delete_start (context, publicId) {
    Vue.http.get(serviceUrls.deleteStart + publicId).then(function (response) {
      alert(response.data.message)
      context.commit('GET_DELETED_STARTS', response.data)
      router.push({
        name: 'Module.DeletedStarts'
      })
      VueNotifications.info({message: response.data.message})
      context.dispatch('loading_false')
    })
  },
  restoreStart (context, publicId) {
    Vue.http.get(serviceUrls.restoreStart + publicId).then(function (response) {
      alert(response.data.message)
      context.commit('GET_ALL_STARTS', response.data)
      router.push({
        name: 'Module.Starts'
      })
      VueNotifications.info({message: response.data.message})
      context.dispatch('loading_false')
    })
  }
}
