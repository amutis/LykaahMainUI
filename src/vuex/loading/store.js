import mutations from './mutations'
import actions from './actions'

const state = {
  loading: false
}

export default {
  state, mutations, actions
}
