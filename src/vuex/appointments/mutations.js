export default {
  GET_NEW_APPOINTMENTS (state, data) {
    state.appointments = data.data
  },
  GET_OLD_APPOINTMENTS (state, data) {
    state.appointments = data.data
  },
  GET_START (state, data) {
    state.start = data.data
  },
  GET_DELETED_STARTS (state, data) {
    state.deleted_starts = data.data
  },
  BRANCH_COUNT (state, data) {
    state.branch_count = data
  }
}
