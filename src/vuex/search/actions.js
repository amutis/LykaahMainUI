import Vue from 'vue'
import {searchUrl} from '../defaults/.url'
import router from '../../router/index'

export default {
  do_search (context, data) {
    // context.dispatch('search_branches', data)
    // context.dispatch('search_services', data)
    router.push({
      name: 'Results'
    })
  },
  search_branches (context, data) {
    context.errors = []
    const postData = {
      search: data
    }
    Vue.http.post(searchUrl.institutionServiceUrl, postData).then(function () {
      router.push({
        name: 'Results'
      })
      context.dispatch('loading_false')
    }).catch(function (error) {
      context.commit('UPDATE_ERRORS', error.data)
      context.dispatch('loading_false')
      // console.log(error.data)
    })
  },
  search_services (context, data) {
    context.errors = []
    const postData = {
      search: data
    }
    Vue.http.post(searchUrl, postData).then(function () {
      router.push({
        name: 'Results'
      })
      context.dispatch('loading_false')
    }).catch(function (error) {
      context.commit('UPDATE_ERRORS', error.data)
      context.dispatch('loading_false')
      // console.log(error.data)
    })
  }
}
