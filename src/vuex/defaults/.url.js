
export const mainUrl = 'http://aumra.lykaah.com/'

export const authUrls = {
  login: mainUrl + 'oauth/token',
  user_details: mainUrl + 'api/auth_user'
}

export const getCategories = mainUrl + 'api/outlets/types'
export const getCategory = mainUrl + 'api/outlets/type/'

const institutionServiceUrl = 'http://lykaah.com:5100/'
// const institutionServiceUrl = 'http://localhost:8000/'
export const searchUrl = {
  // findServices: mainUrl + 'institutions/post',
  findBranches: institutionServiceUrl + 'search'
}

export const institutionUrls = {
  postInstitution: institutionServiceUrl + 'institution/add',
  postBranch: institutionServiceUrl + 'branch/add',
  getInstitutions: institutionServiceUrl + 'institutions',
  getInstitution: institutionServiceUrl + 'institution/',
  deleteInstitution: institutionServiceUrl + 'institutions/delete/',
  editInstitution: institutionServiceUrl + 'institutions/edit/',
  getInstitutionTypes: institutionServiceUrl + 'institution/types',
  getBranches: institutionServiceUrl + 'my-branches/',
  getNearBranches: institutionServiceUrl + 'branches',
  getBranch: institutionServiceUrl + 'branch/'
}

export const userUrls = {
  postInstitutionUser: mainUrl + 'api/add/institution/user',
  postBranchUser: mainUrl + 'api/add/branch/user',
  updateManagerStatus: institutionServiceUrl + 'manager/',
  getBranchUsers: mainUrl + 'api/branch/users/',
  postUser: mainUrl + 'api/add/user',
  deleteUser: mainUrl + 'users/delete/',
  editUser: mainUrl + 'users/edit/',
  getAllUsers: mainUrl + 'users/all',
  restoreUser: mainUrl + 'users/restore/',
  countUsers: mainUrl + 'api/count/users',
  getDeletedUser: mainUrl + 'users/deleted'
}

const servicesServiceUrl = 'http://lykaah.com:5101/'

export const serviceUrls = {
  postService: servicesServiceUrl + 'service/add',
  getBranchServices: servicesServiceUrl + 'branch/services/',
  getStart: mainUrl + 'starts/single/',
  deleteStart: mainUrl + 'starts/delete/',
  editStart: mainUrl + 'starts/edit/',
  getAllStarts: mainUrl + 'starts/all',
  restoreStart: mainUrl + 'starts/restore/',
  getDeletedStart: mainUrl + 'starts/deleted'
}

const appointmentServiceUrl = 'http://lykaah.com:5102/'
// const appointmentServiceUrl = 'http://localhost:8000/'
export const appointmentUrls = {
  postCode: appointmentServiceUrl + 'confirm',
  postAppointment: appointmentServiceUrl + 'appointment/add',
  getNewAppointments: appointmentServiceUrl + 'appointments/not-confirmed/',
  getOldAppointments: appointmentServiceUrl + 'appointments/confirmed/',
  getStart: mainUrl + 'starts/single/',
  branchCount: appointmentServiceUrl + 'count/branch/appointments/',
  deleteStart: mainUrl + 'starts/delete/',
  editStart: mainUrl + 'starts/edit/',
  getAllStarts: mainUrl + 'starts/all',
  restoreStart: mainUrl + 'starts/restore/',
  getDeletedStart: mainUrl + 'starts/deleted'
}
