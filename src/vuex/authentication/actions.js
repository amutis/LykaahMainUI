import Vue from 'vue'
import router from '../../router/index'
import {clientId, clientSecret} from '../defaults/store'
import {authUrls} from '../defaults/.url'
import VueNotifications from 'vue-notifications'

export default {
  do_login (context, loginData) {
    const postData = {
      username: loginData.email,
      password: loginData.password,
      grant_type: 'password',
      client_id: clientId,
      client_secret: clientSecret,
      scope: ''
    }
    Vue.http.post(authUrls.login, postData).then(function (response) {
      if (response.status !== 200) {
        VueNotifications.error({message: 'Failed Login Attempt'})
        // alert('Wrong Credentials')
      } else {
        localStorage.setItem('access_token', response.data.access_token)
        context.dispatch('get_credentials')
        VueNotifications.success({message: 'Login Success'})
      }
    }).catch(function (error) {
      context.commit('UPDATE_ERRORS', error.data)
      VueNotifications.error({message: 'Failed Login Attempt'})
      context.dispatch('loading_false')
      // var notification = {
      //   title: 'Login Success',
      //   text: 'Welcome'
      // }
      // context.commit('UpdateNotifications', notification)
      // console.log(error.data)
    })
  },
  get_credentials (context) {
    Vue.http.get(authUrls.user_details).then(function (response) {
      if (response.status !== 200) {
        alert('Wrong Credentials')
      } else {
        context.commit('GET_STATE', response.data)
        context.dispatch('find_view')
        VueNotifications.info({message: 'Details Gotten'})
      }
    })
  },
  find_view (context) {
    const userType = context.state.session.user_type_id
    localStorage.setItem('mnbvcxz', userType)
    localStorage.setItem('user_name', context.state.session.first_name + ' ' + context.state.session.last_name)
    localStorage.setItem('lkjhgfdsa', context.state.session.public_id)
    if (userType === 'D3KZBwCb') {
      localStorage.setItem('institution', context.state.session.institution_id)
      // Institution
      router.push({
        name: 'Dashboard'
      })
    } else if (userType === 'BDf0F5vu') {
      // Branch
      localStorage.setItem('institution', context.state.session.institution_id)
      localStorage.setItem('branch', context.state.session.branch_id)
      router.push({
        name: 'Dashboard'
      })
    } else if (userType === '') {
      // Employee
    } else if (userType === 'hpciGVHt') {
      router.push({
        name: 'Institution.Index'
      })
      // Villager
    }
  },
  reset_storage (context) {
    localStorage.clear()
    window.location.replace('/')
  }
}
